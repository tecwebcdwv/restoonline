<?php
require '../inc/config.php';
require '../inc/db_connection.php';
require_once '../inc/secure_session.php';

secure_session_start();

if (user_check_login() && isset($_POST["id_notifica"])) {
  (int)$id = $_POST["id_notifica"];


  $email = $_SESSION["email"];
  $sql = "SELECT * FROM users WHERE email = '$email'";
  $userquery = $conn->query($sql);

  if ($userquery && $userquery->num_rows > 0) {
    $userrows = $userquery->fetch_assoc();
    $user_id = $userrows["id"];


    $sql = "DELETE FROM `usernotifications` WHERE id = $id AND user_id = $user_id";
    $deletequery = $conn->query($sql);

    if ($deletequery) {

      // check if there are other notifications
      $sql = "SELECT * FROM  usernotifications WHERE user_id = $user_id";
      $userquery2 = $conn->query($sql);

      if ($userquery2 && $userquery2->num_rows <= 0) {

        // set hasnotifications to false to user
        $sql = "UPDATE users SET hasnotifications = 0 WHERE id = $user_id";
        $updatequery = $conn->query($sql);
        if ($updatequery) {
          // ok
          echo 1;
          exit;
        } else {
          echo 0;
          exit;
        }
      } // else has other notifications
    } // else user not found
  } else {
    echo 0;
  }
}
?>
