<?php
require '../inc/config.php';
require '../inc/db_connection.php';
require_once '../inc/secure_session.php';

secure_session_start();

if (admin_check_login() && isset($_POST["id_notifica"])) {
  (int)$id = $_POST["id_notifica"];

  $sql = "DELETE FROM `adminnotifications` WHERE id = $id";
  $adminquery = $conn->query($sql);

  if ($adminquery) {

    // check if there are other notifications
    $sql = "SELECT * FROM  adminnotifications";
    $adminquery2 = $conn->query($sql);

    if ($adminquery2 && $adminquery2->num_rows <= 0) {

      // set hasnotifications to false to admin(s)
      $sql = "UPDATE users SET hasnotifications = 0 WHERE role_id = 2";
      $updatequery = $conn->query($sql);
      if ($updatequery) {
        // ok
        echo 1;
        exit;
      } else {
        echo 0;
        exit;
      }
    } // else has other notifications
  } else {
    echo 0;
  }
}
?>
