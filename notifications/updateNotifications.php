<?php
require '../inc/config.php';
require '../inc/db_connection.php';
require_once '../inc/secure_session.php';

secure_session_start();
if(!user_check_login()) {
  header("location: ./index.php");
  exit;
}

if (user_check_login()) {

  $email = $_SESSION["email"];
  $sql = "SELECT * FROM users WHERE email = '$email'";
  $userquery = $conn->query($sql);

  if ($userquery && $userquery->num_rows > 0) {
    $userrows = $userquery->fetch_assoc();
    $user_id = $userrows["id"];


    $sql = "SELECT * FROM users WHERE id = $user_id AND hasnotifications = 1"; // cerco se l'utente ha le notifiche
    $usernotifquery = $conn->query($sql);

    if ($usernotifquery && $usernotifquery->num_rows > 0) {	// se l'utente ha notifiche da mostrare

      $sql = "SELECT * FROM usernotifications WHERE user_id = $user_id ORDER BY created_at DESC"; // cerco le notifiche dell'admin
      $userquery = $conn->query($sql);
      if ($userquery && $userquery->num_rows > 0) {
          while($notifica = $userquery->fetch_assoc()) { ?>

            <li class="nav-item">
              <div class="alert alert-success alert-dismissable">
                <a href="#" onclick="cancellaNotificaUser(<?php echo $notifica["id"]; ?>)" class="close" data-dismiss="alert" aria-label="close">x</a>
                <strong><?php echo $notifica["name"]; ?></strong> <?php echo $notifica["description"]; ?>
              </div>
            </li>
          <?php
        }
      }
    }
  }
}
?>
