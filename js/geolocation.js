function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);

    } else {
      //  x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
  var x = document.getElementById("inputAddress");
  $.ajax({
    url: "inc/geolocation_checker.php?latitude=" + position.coords.latitude + "&longitude=" + position.coords.longitude
  }).done(function(data) {
    if(data) {
      x.value = data;
    } else {
    //  x.value = "servizio non disponibile";
      alert("Servizio non disponibile sulla tua locazione");
    }

  });
}
