<?php
require 'inc/config.php';
require 'inc/db_connection.php';
require_once 'inc/secure_session.php';

secure_session_start();
if(!user_check_login()) {
  header("location: login.php");
  exit;
}

if (!isset($_POST["product_id"])) {
  header("location: index.php");
  exit;
}


$post_id = $_POST["product_id"];
$email = $_SESSION["email"];
$sql = "SELECT * FROM users WHERE email = '$email'";
$userquery = $conn->query($sql);

if ($userquery && $userquery->num_rows > 0) {
  $userrows = $userquery->fetch_assoc();
  $user_id = $userrows["id"];

  $cart_id = 0;

  $sql = "SELECT id, user_id FROM carts WHERE user_id = $user_id";
  $cartquery = $conn->query($sql);

  if ($cartquery->num_rows <= 0) {
    // manca l'elemento cart per l'user corrente, cioò questo è il primo elemento del carrello


    $now = date(DATE_W3C);
    $sql = "INSERT INTO carts(user_id, created_at, updated_at) VALUES ('$user_id', '$now', '$now')";
    $cartinsertquery = $conn->query($sql);

    if (!$cartinsertquery) {
      ?><script type="text/javascript">
        alert("Failed cart insert.");
        </script><?php
        exit;
    }
    // ho inserito il cart

    $sql = "SELECT id, user_id FROM carts WHERE user_id = $user_id";
    $cartquery = $conn->query($sql);
    if ($cartquery && $cartquery->num_rows > 0) {
      $cartrow = $cartquery->fetch_assoc();
      $cart_id = $cartrow["id"];
    }

    $sql = "INSERT INTO cartitems(cart_id, product_id, quantity) VALUES ($cart_id, $post_id, 1)";
    $iteminsert = $conn->query($sql);
    if ($iteminsert) {
      // ho inserito l'item nel carrello

      $_SESSION["cart_present"] = true;

      header("location: index.php");
      exit;
    }

  } else {
    $cartrow = $cartquery->fetch_assoc();
    $cart_id = $cartrow["id"];
  }


  // cerco un cartitem del prodotto, e c'è aumento il quantity, altrimenti ne creo uno nuovo
  $sql = "SELECT `id`, `cart_id`, `product_id`, `quantity` FROM `cartitems` WHERE cart_id = $cart_id AND product_id = $post_id";

  $itemquery = $conn->query($sql);


  if ($itemquery->num_rows <= 0) {
    $sql = "INSERT INTO `cartitems`(`cart_id`, `product_id`, `quantity`) VALUES ($cart_id, $post_id, 1)";
    $iteminsert = $conn->query($sql);
    if ($iteminsert) {
      // ho inserito l'item nel carrello
      header("location: index.php");
      exit;
    } else {
      ?><script type="text/javascript">
        alert("Failed cartitem insert.");
        </script><?php
    }
  } else {  //c'è il prodotto nel carrello, faccio l'update del valore quantity
    $item = $itemquery->fetch_assoc();
    $item_id = $item["id"];
    $item_quantity = $item["quantity"] + 1;

    $sql = "UPDATE `cartitems` SET quantity = $item_quantity WHERE cart_id = $cart_id AND product_id = $post_id";
    $itemupdate = $conn->query($sql);

    if ($itemupdate) {
      // ho aggiornato la quantità dell'item
      header("location: index.php");
      exit;
    }
  }

} else { // failed login
  ?><script type="text/javascript">
    alert("Failed user id search.");
    </script><?php

}






?>
