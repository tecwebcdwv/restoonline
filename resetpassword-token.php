<?php
require_once 'inc/config.php';
require 'inc/db_connection.php';
require 'inc/secure_session.php';

secure_session_start();
if(isset($_POST["email"]) && isset($_POST["token"]) ) {




  $email = $_POST["email"];
  $token = $_POST["token"];

  $email_check = $conn->query("SELECT * FROM users where email = '$email' AND remember_token = '$token'");
  if($email_check && $email_check->num_rows > 0) {

    $_SESSION["resetter-email"] = $email;   // to unset after password reset
    $_SESSION["resetter-token"] = $token;

    header("location: resetpassword-newpassword.php");

    } else {
      ?> <script type="text/javascript">
       location.href = "resetpassword-token.php";
       alert("Dati non validi.");
      </script>
      <?php
    }
    $conn->close();
}

?>

<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Registrati a <?php echo "$APP_NAME"; ?></title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">

  	<link href="css/style.css" rel="stylesheet">
    <link href="css/sign.css" rel="stylesheet">


  </head>

  <body>
    <?php include 'header.php'; ?>

    <div class="container">

      <form class="form-signin" method="POST" action="resetpassword-token.php">
        <h2 class="form-signin-heading">Inserisci l'email e il codice che hai ricevuto via e-mail</h2>
        <label for="inputEmail" class="sr-only">Email</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Email" name="email" required>
        <label for="inputPassword" class="sr-only">Codice di verifica</label>
        <input type="text" id="inputPassword" class="form-control" placeholder="Token" name="token" required>
        <div>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit" name="register">Invia</button>

      </form>

    </div>
    <?php include 'footer.php'; ?>
    </body>
  </html>
