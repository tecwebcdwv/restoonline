<?php
/*
Funzioni per gestire la sessione dell'utente e admin in modo sicuro
*/
function debug_to_console( $data ) {
    $output = $data;
    if ( is_array( $output ) )
        $output = implode( ',', $output);

    echo "<script>console.log( 'Debug Objects: " . $output . "' );</script>";
}

function getToken($length){
     $token = "";
     $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
     $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
     $codeAlphabet.= "0123456789";
     $max = strlen($codeAlphabet); // edited

    for ($i=0; $i < $length; $i++) {
        $token .= $codeAlphabet[random_int(0, $max-1)];
    }

    return $token;
}

function secure_session_start() {
        $session_name = 'secure_session_id';
        $secure = false; // true se 'https'.
        $httponly = true; // impedisce ad un javascript di accedere all'id di sessione.
        ini_set('session.use_only_cookies', 1); // Forza la sessione ad utilizzare solo i cookie.
        $cookieParams = session_get_cookie_params(); // Legge i parametri correnti
        session_set_cookie_params(
          $cookieParams["lifetime"], $cookieParams["path"],
          $cookieParams["domain"], $secure,
          $httponly); // aggiunge ai parametri quelli persnalizzati
        session_name($session_name); // Imposta il nome di sessione
        session_start(); // Avvia la sessione php.
        session_regenerate_id(); // Rigenera la sessione
}

function login($email, $password, $mysqli) {
   // uso lo statement prepare per proteggere da SQL injection.
   if ($statement = $mysqli->prepare(
     "SELECT id, name, email, password, role_id FROM users WHERE email = ? LIMIT 1")) {
      $statement->bind_param('s', $email); // esegue il bind del parametro email.
      $statement->execute(); // esegue la query
      $statement->store_result();
      $statement->bind_result($user_id, $name, $email, $db_password, $roleId); // recupera il risultato della query e lo memorizza nelle relative variabili.
      $statement->fetch();

      if($statement->num_rows == 1) { // se l'utente esiste

        if (password_verify($password, $db_password)) { // Verifica che la password memorizzata nel database corrisponda alla password fornita dall'utente.
            // Password corretta
             $_SESSION['uid'] = $user_id;
             $_SESSION['name'] = $name;
             $_SESSION['email'] = $email;

             if($roleId == 2) { // se è admin
               $_SESSION['role'] = 'admin';
             } else {
               $_SESSION['role'] = 'user';
             }
            // Login eseguito con successo.
            return true;
         } else {
           // password errata
            return false;
         }
      } else {
         // L'utente inserito non esiste.
         return false;
      }
   }
}

function check_login() {
   return isset($_SESSION["name"]);
}

function user_check_login() {
  return check_login() && $_SESSION["role"] === 'user';
}

function admin_check_login() {
  return check_login() && $_SESSION["role"] === 'admin';
}
?>
