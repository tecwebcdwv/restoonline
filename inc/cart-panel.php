
  <ul class="list-group">

    <li class="list-group-item active cart">
      <div class="flex-container" style="display:flex">
        <div style="flex-grow: 20">Carrello</div>

        <div style="flex-grow: 1">Totale: € </div>
        <div class="cart-totale"></div></div>

    </li>

    <?php
    $email = $_SESSION["email"];
    $sql = "SELECT * FROM users WHERE email = '$email'";
    $userquery = $conn->query($sql);

    if ($userquery && $userquery->num_rows > 0) {
      $userrows = $userquery->fetch_assoc();
      $user_id = $userrows["id"];

      $sql = "SELECT id, user_id FROM `carts` WHERE user_id = $user_id";
      $cart = $conn->query($sql);


      if ($cart->num_rows <= 0) { ?>
        <li class="list-group-item">
          <div class="flex-container">
            <div style="flex-grow: 20">Il carrello è vuoto.</div>
          </div>
        </li>

      <?php
      } else {
        $cart = $cart->fetch_assoc();
        $cart_id = $cart["id"];
        $sql = "SELECT product_id, name, price, quantity FROM cartitems, products WHERE cartitems.cart_id = $cart_id AND products.id = cartitems.product_id";
        $itemsquery = $conn->query($sql);

        if ($itemsquery && $itemsquery->num_rows > 0) {
          $_SESSION["cart_present"] = true;
          // output data of each row

          while($prods = $itemsquery->fetch_assoc()) { ?>

            <li class="list-group-item">
              <div class="flex-container cart-product" style="display:flex">
                <div class="flex-item product-quantity" style="flex-grow: 1"><?php echo $prods["quantity"]; ?></div>
                <div class="flex-item product-name" style="flex-grow: 25"><?php echo "x ".$prods["name"];?></div>
                <div class="flex-item euro-sign">€</div>
                <div class="flex-item product-price" ><?php echo $prods["price"];?></div>

                <div class="flex-item add-to-cart">
                  <form action="removeFromCart.php" method="post">
                    <button class="btn btn-secondary add-to-cart" name="product_id" value="<?php echo $prods["product_id"];?>">
                      <em class="fa fa-minus fa fa-fw" aria-hidden="true"></em>
                    </button>
                  </form>

                </div>

              </div>
            </li>

          <?php
        }
        ?>
      <li class="flex-container ordina-button">
          <a href="confermaDati.php" class="btn btn-lg btn-outline-primary btn-block" role="button" aria-pressed="true">Ordina</a>
      </li>
<?php
    } else {
      //carrello esistente ma vuoto
    }
  }
}
    ?>


  </ul>
