<?php

if (isset($_GET["latitude"]) && isset($_GET["longitude"])) {

  $lat = $_GET["latitude"];
  $long = $_GET["longitude"];
  $coords = "$lat,$long";

  echo check_coordinates($coords);
}


function check_address($address) {
  // replace spaces with plus to create a valid GET parameter
  $my_address = str_replace(' ', '+', $address);
  $apikey = "AIzaSyAnub0gLOmdzmi61IYlteXndH5MPSO91EY";

  $jsonurl = "https://maps.googleapis.com/maps/api/geocode/json?key=$apikey&address=$my_address";

  return check_city_and_number($jsonurl);
}

function check_coordinates($coordinates) {

  $apikey = "AIzaSyAnub0gLOmdzmi61IYlteXndH5MPSO91EY";
  $jsonurl = "https://maps.googleapis.com/maps/api/geocode/json?key=$apikey&latlng=$coordinates";

// testing: le coordinate sono di via sacchi 3 cesena
//  $jsonurl = "https://maps.googleapis.com/maps/api/geocode/json?key=$apikey&latlng=44.139761,12.243219";

  return check_city_and_number($jsonurl);
}

function check_city_and_number($jsonurl) {
  // get the json from Google geocoding system into a string
  $json = file_get_contents($jsonurl);

  // decode the string into associative array
  $obj = json_decode( $json, true );

  $civico = false;
  $adr = false;

  if ($obj["status"] == "OK") {
    $result = $obj["results"][0];
    if (isset($result["address_components"])) {
      foreach($result["address_components"] as $item) {
        if (isset($item["types"]) && isset($item["short_name"]) && isset($item["long_name"])){

          if ($item["types"][0] === "street_number") {
            $civico = true;
          }
      		if ($item["types"][0] === "locality" && $item["long_name"] == "Cesena") {
      			$adr = $result["formatted_address"];
      		}
        }
      }
    }
  }
  if ($civico && $adr) {
    return $adr;
  }else {
  return false;
  }
}

?>
