<?php
require 'inc/config.php';
require 'inc/db_connection.php';
require_once 'inc/secure_session.php';
secure_session_start();

?>

<!DOCTYPE html>
<html lang="it">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

	<title><?php echo "$APP_NAME"; ?></title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

	<link href="css/style.css" rel="stylesheet">
	<script src="js/updateNotifications.js"></script>
</head>
<body>
<?php
  include 'header.php';
?>


	<div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-12">




        <h1>Privacy Policy</h1>

          <div class="entry-content">
        <h2>Privacy policy per i visitatori del sito</h2>
        <p>La presente Privacy Policy ha lo scopo di descrivere le modalità di gestione di questo sito, in riferimento al trattamento dei dati personali degli utenti/visitatori che lo consultano.
          Si tratta di un’informativa che è resa anche ai sensi dell’art. 13 del D. Lgs. 196/03 – Codice in materia di protezione dei dati personali – a coloro che si collegano al sito web www.restoonline.it e usufruiscono dei relativi servizi web a partire dall’indirizzo www.restoonline.it.
          L’informativa è resa soltanto per i siti sopra menzionati e non anche per altri siti web eventualmente consultati dall’utente tramite appositi link. Il sito www.restoonline.it è di proprietà di RestoOnline, gestito da <a href="index.php" target="_blank" rel="noopener">RestoOnline</a> che garantisce il rispetto della normativa in materia di protezione dei dati personali (D. Lgs. 196/03).
          Gli utenti/visitatori dovranno leggere attentamente la presente Privacy Policy prima di inoltrare qualsiasi tipo di informazione personale e/o compilare qualunque modulo elettronico presente sul sito stesso.</p>

        <h2>Tipologia di dati trattati e finalità del trattamento</h2>
        <h3>Dati di navigazione</h3>
        <p>I sistemi informatici e le procedure software preposte al funzionamento di questo sito acquisiscono, nel normale esercizio, alcuni dati personali che vengono poi trasmessi implicitamente nell’uso dei protocolli di comunicazione Internet. Si tratta di informazioni che non sono raccolte per essere associate a interessati identificati, ma che per loro stessa natura potrebbero, attraverso elaborazioni e associazioni con dati detenuti da terzi, permettere di identificare gli utenti. In questa categoria di dati rientrano gli indirizzi IP o i nomi a dominio dei computer utilizzati dagli utenti che si connettono al sito, gli indirizzi in notazione URI (Uniform Resource Identifier) delle risorse richieste, l’orario della richiesta, il metodo utilizzato nel sottoporre la richiesta al server, la dimensione del file ottenuto in risposta, il codice numerico indicante lo stato della risposta data dal server (buon fine, errore, ecc.) ed altri parametri relativi al sistema operativo e all’ambiente informatico dell’utente. Questi dati vengono utilizzati al solo fine di ricavare informazioni statistiche anonime sull’uso del sito e per controllarne il corretto funzionamento e vengono cancellati immediatamente dopo l’elaborazione. I dati potrebbero essere utilizzati per l’accertamento di responsabilità in caso di ipotetici reati informatici ai danni del sito: salva questa eventualità, i dati sui contatti web non persistono per più di sette giorni.</p>

        <p><strong>Dati forniti volontariamente dagli utenti/visitatori</strong></p>
        <p>Qualora gli utenti/visitatori, collegandosi a questo sito, inviino propri dati personali per accedere a determinati servizi, ovvero per effettuare richieste in posta elettronica, ciò comporta l’acquisizione da parte di RestoOnline dell’indirizzo del mittente e/o di altri eventuali dati personali che verranno trattati esclusivamente per rispondere alla richiesta, ovvero per la fornitura del servizio.
          I dati personali forniti dagli utenti/visitatori verranno comunicati a terzi solo nel caso in cui la comunicazione stessa sia necessaria per ottemperare alle richieste degli utenti/visitatori medesimi.</p>

        <h3>Informativa estesa sull’utilizzo dei cookie</h3>
        <p>Per rendere i propri servizi il più possibile efficienti e semplici da utilizzare questo sito internet utilizza i cookie. Il cookie è un piccolo file di testo che viene registrato dal browser e permette di memorizzare dati delle pagine web e delle sessioni del browser. Visitando e navigando questo sito, viene inserita una quantità minima di informazioni nel dispositivo dell’utente. Questi cookie vengono salvati nella directory del browser Web dell’Utente. Come definito nel provvedimento del Garante sono state individuate due macro-categorie di cookie: cookie “tecnici” e cookie “di profilazione”. (<a href="http://www.garanteprivacy.it/web/guest/home/docweb/-/docweb-display/docweb/3118884">Per approfondimenti leggi il documento sul sito del Garante</a>)</p>
        <p></p>
        <h3>Tipologia di cookie utilizzati da questo sito</h3>
        <p><strong>Cookie tecnici</strong></p>
        <p>Cookie tecnici di navigazione o di sessione sono utilizzati per garantire la normale fruizione del sito internet, per poter acquistare e autenticarsi per accedere alle aree riservate del sito oltre che per monitorarne il corretto funzionamento.</p>
        <p>Cookie di funzionalità migliorano il servizio dell’utente in quanto consentono la navigazione in funzione di una serie di criteri selezionati (ad esempio, la lingua, i prodotti selezionati per l’acquisto).</p>
        <p>Cookie analytics utilizzati direttamente dal titolare di questo sito per raccogliere informazioni, in forma aggregata, sul numero degli utenti e su come questi visitano il sito.</p>
        <p>Per l’installazione di tali cookie non è richiesto il preventivo consenso degli utenti, mentre resta fermo l’obbligo di dare l’informativa ai sensi dell’art. 13 del Codice, che il gestore del sito, qualora utilizzi soltanto tali dispositivi, potrà fornire con le modalità che ritiene più idonee.</p>
        <p></p>

        <h3>Come gestire i cookie tramite il browser</h3>
        <p>In molti browser è possibile impostare le regole per gestire i cookie sito per sito, opzione che offre un controllo più mirato della propria privacy. E’ possibile disabilitare i cookie di tutti i siti, ad eccezione di quelli ritenuti affidabili.</p>
        <p>Tutti i browser, nel menu Strumenti contengono l’opzione Cancella dati di navigazione. Utilizzare questa opzione per eliminare i cookie e altri dati di siti e plug-in, inclusi i dati memorizzati nel proprio dispositivo da Adobe Flash Player (comunemente noti come cookie Flash). Per istruzioni dettagliate consultate le istruzioni del browser utilizzato. Qui di seguito le istruzioni per i browser più utilizzati:</p>
        <p><a href="http://support.microsoft.com/kb/196955">Internet Explorer</a><br />
        <a href="http://support.mozilla.com/en-US/kb/Cookies">Firefox</a><br />
        <a href="https://support.google.com/chrome/bin/answer.py?hl=en&amp;answer=95647&amp;p=cpn_cookies">Chrome</a><br />
        <a href="http://support.apple.com/kb/PH5042">Safari</a> and <a href="http://support.apple.com/kb/HT1677">iOS</a></p>
        <p>Un’altra funzione presente nei browser più recenti (e.g. Internet Explorer, Chrome, Firefox, Safari) è la modalità di navigazione in incognito. Si può navigare in modalità incognita per evitare che le visite ai siti web o i download vengano registrati nelle varie cronologie. Con la modalità di navigazione in incognito tutti i cookie creati vengono eliminati dopo la chiusura di tutte le finestre di navigazione in incognito.</p>

        <h3 class="crafty-social-buttons crafty-social-share-buttons crafty-social-buttons-size-2 crafty-social-buttons-align-center crafty-social-buttons-caption-inline-block">Modalità del trattamento</h3>
        <p>I dati personali sono trattati con strumenti automatizzati per il tempo strettamente necessario a conseguire gli scopi per cui sono stati raccolti. Specifiche misure di sicurezza sono osservate per prevenire la perdita dei dati, usi illeciti o non corretti ed accessi non autorizzati. Facoltatività del conferimento dei dati, a parte quanto specificato per i dati di navigazione, gli utenti/visitatori sono liberi di fornire i propri dati personali. Il loro mancato conferimento può comportare unicamente l’impossibilità di ottenere quanto richiesto.</p>
        <p><strong>Luogo di trattamento dei dati</strong><br />
        I trattamenti connessi ai servizi web dei siti sopra indicati, hanno luogo presso RestoOnline, in Via Tal dei tali 7, Cesena del Titolare del trattamento e/o presso la sede della società di hosting e/o gestione del sito web e sono curati solo da personale tecnico dell’Ufficio incaricato del trattamento, oppure da eventuali incaricati di occasionali operazioni di manutenzione.
         Nessun dato derivante dal servizio web viene comunicato o diffuso.
         I dati personali forniti dagli utenti che inoltrano richieste di invio di materiale informativo sono utilizzati al solo fine di eseguire il servizio o la prestazione richiesta e sono comunicati a terzi nel solo caso in cui ciò sia a tal fine necessario.</p>
        <p><strong>Titolare del trattamento</strong><br />
        Il titolare del trattamento dei dati personali è RestoOnline.</p>
        <p><strong>Diritti degli interessati</strong><br />
        I soggetti cui si riferiscono i dati personali hanno il diritto in qualunque momento di ottenere la III conferma dell’esistenza o meno dei medesimi dati e di conoscerne il contenuto e l’origine, verificarne l’esattezza o chiederne l’integrazione l’aggiornamento, oppure la rettificazione (art. 7 del D. Lgs. 196/03). Ai sensi del medesimo articolo si ha il diritto di chiedere la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in violazione di legge, nonché di opporsi in ogni caso, per motivi legittimi, al loro trattamento. Le richieste vanno inoltrate al Titolare del trattamento ai recapiti indicati sopra.</p>

        </div>
      </div>
    </div>
  </div>   <!-- fine container fluid-->

    <?php include 'footer.php'; ?>
  </body>
</html>
