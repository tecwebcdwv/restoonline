<?php

if (!isset($APP_NAME)) {
	die('Direct access not permitted');
}
 ?>



<header>

	<nav class="navbar navbar-expand-md navbar-light bg-light">

		<button class="navbar-toggler navbar-toggler-left d-lg-none" type="button" data-toggle="collapse" data-target="#navbarLinks" aria-controls="navbarLinks" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
    </button>
		<a class="navbar-brand" href="index.php"><?php echo "$APP_NAME"; ?></a>
		<?php
		$notifichebuttonclass = "fa fa-bell-o fa-6 notify-toggler-icon";	// senza notifiche
		$notificheadmin = false;
		$notificheuser = false;
		$notifications_user_id = 0;

		if (admin_check_login()) {


			$sql = "SELECT * FROM users WHERE role_id = 2 AND hasnotifications = 1"; // cerco se l'admin ha le notifiche
	    $adminquery = $conn->query($sql);

	    if ($adminquery && $adminquery->num_rows > 0) {	// se l'admin ha notifiche da mostrare
				$notifichebuttonclass = "fa fa-bell fa-6 notify-toggler-icon";
				$notificheadmin = true;
			}
		} else if (user_check_login()) {

			$email = $_SESSION["email"];
	    $sql = "SELECT * FROM users WHERE email = '$email'";
	    $userquery = $conn->query($sql);

	    if ($userquery && $userquery->num_rows > 0) {
	      $userrows = $userquery->fetch_assoc();
	      $user_id = $userrows["id"];


				$sql = "SELECT * FROM users WHERE id = $user_id AND hasnotifications = 1"; // cerco se l'utente ha le notifiche
		    $usernotifquery = $conn->query($sql);

		    if ($usernotifquery && $usernotifquery->num_rows > 0) {	// se l'utente ha notifiche da mostrare
					$notifichebuttonclass = "fa fa-bell fa-6 notify-toggler-icon";
					$notificheuser = true;
					$notifications_user_id = $user_id;
				}
			}
		}
			?>
				<button class="navbar-toggler navbar-toggler-left d-lg-none" id="buttonnotifiche" type="button" data-toggle="collapse" data-target="#notifiche" aria-controls="notifiche" aria-expanded="false" aria-label="Toggle notifiche">
					<em class="<?php echo $notifichebuttonclass; ?>"></em>
		    </button>


		<div class="collapse navbar-collapse" id="navbarLinks">
			<ul class="navbar-nav mr-auto">
				<?php
				if (user_check_login()) {?>
				<li class="nav-item">
					<a class="nav-link" href="cart.php">Carrello</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="my-orders.php">Ordini</a>
				</li>

			<?php } else if (admin_check_login()) { ?>
				<li class="nav-item">
					<a class="nav-link" href="index.php">Ordini</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="listino.php">Listino</a>
				</li>
				<?php
			} ?>

			</ul>
			<?php
			if (!check_login()) {?>
			<div class="btn-group" role="group" aria-label="account buttons">
						<a class="btn btn-outline-secondary" href="login.php" role="button">Login</a>
						<a class="btn btn-outline-secondary" href="register.php" role="button">Registrati</a>
			</div>
			<?php
		} else {?>
			<div class="header-menu">
				<div class="btn-group">
					<button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown">Benvenuto, <?php echo $_SESSION['name']; ?></button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="logout.php">Logout</a>
					</div>
				</div>
			</div>
			<?php
		} ?>

  	</div>


	</nav>

	<div class="collapse navbar-collapse col-sm-12 col-md-12 col-lg-12" id="notifiche">
		<ul class="navbar-nav mr-auto" id="listanotifiche">
			<?php
			if (admin_check_login() && $notificheadmin) {	// se è loggato l'admin e ha notifiche da mostrare

				$sql = "SELECT * FROM adminnotifications ORDER BY created_at DESC"; // cerco le notifiche dell'admin
		    $adminquery = $conn->query($sql);

		    if ($adminquery && $adminquery->num_rows > 0) {

					  while($notifica = $adminquery->fetch_assoc()) { ?>

							<li class="nav-item">
								<div class="alert alert-success alert-dismissable">
									<a href="#" onclick="cancellaNotificaAdmin(<?php echo $notifica["id"]; ?>)" class="close" data-dismiss="alert" aria-label="close">x</a>
									<strong><?php echo $notifica["name"]; ?></strong> <?php echo $notifica["description"]; ?>
								</div>
							</li>
						<?php
					}
				}
			} else if (user_check_login() && $notificheuser) {
				$sql = "SELECT * FROM usernotifications WHERE user_id = $notifications_user_id ORDER BY created_at DESC"; // cerco le notifiche dell'admin
		    $userquery = $conn->query($sql);
				if ($userquery && $userquery->num_rows > 0) {
					  while($notifica = $userquery->fetch_assoc()) { ?>

							<li class="nav-item">
								<div class="alert alert-success alert-dismissable">
									<a href="#" onclick="cancellaNotificaUser(<?php echo $notifica["id"]; ?>)" class="close" data-dismiss="alert" aria-label="close">x</a>
									<strong><?php echo $notifica["name"]; ?></strong> <?php echo $notifica["description"]; ?>
								</div>
							</li>
						<?php
					}
				}
			}
				 ?>

		</ul>
	</div>



</header>
