<?php
require 'inc/config.php';
require 'inc/db_connection.php';
require_once 'inc/secure_session.php';

secure_session_start();
if(!user_check_login()) {
  header("location: login.php");
  exit;
}

if (!isset($_POST["product_id"])) {
  header("location: index.php");
  exit;
}


$post_id = $_POST["product_id"];
$email = $_SESSION["email"];
$sql = "SELECT * FROM users WHERE email = '$email'";
$userquery = $conn->query($sql);

if ($userquery && $userquery->num_rows > 0) {
  $userrows = $userquery->fetch_assoc();
  $user_id = $userrows["id"];
  $cart_id = 0;

  $sql = "SELECT id, user_id FROM carts WHERE user_id = $user_id";
  $cartquery = $conn->query($sql);

  if ($cartquery->num_rows > 0) {
    // ho il cart
    $cartrow = $cartquery->fetch_assoc();
    $cart_id = $cartrow["id"];
    $now = date(DATE_W3C);


    $sql = "SELECT `id`, `cart_id`, `product_id`, `quantity` FROM `cartitems` WHERE cart_id = $cart_id AND product_id = $post_id";
    $itemquery = $conn->query($sql);

    if ($itemquery && $itemquery->num_rows > 0) {
      // ho il cartitem

      $itemrow = $itemquery->fetch_assoc();
      $quantity = $itemrow["quantity"];
      $item_id = $itemrow["id"];

      if($quantity <= 1) {
        //devo rimuovere il cartitem
        $sql = "DELETE FROM cartitems WHERE id = $item_id";
        $itemdeletequery = $conn->query($sql);

        if ($itemdeletequery) { // ho eliminato l'item
          check_items($cart_id, $conn);
          header("location: index.php");
          exit;
        }
      } else {
        $item_quantity  = $quantity - 1;

        $sql = "UPDATE `cartitems` SET quantity = $item_quantity WHERE cart_id = $cart_id AND product_id = $post_id";
        $itemupdate = $conn->query($sql);

        if ($itemupdate) {
          // ho aggiornato la quantità dell'item

          check_items($cart_id, $conn);
          header("location: index.php");
          exit;
        }

      }

    }





  } else { // non ho il cart

  }

} else { // failed login
  ?><script type="text/javascript">
    alert("Failed user id search.");
    </script><?php

}


function check_items($cart_id, $conn) {

  $sql = "SELECT * FROM cartitems WHERE cart_id = $cart_id";
  $itemquery = $conn->query($sql);

  if ($itemquery->num_rows <= 0) {
    $sql = "DELETE FROM carts WHERE id = $cart_id";
    $deletecartquery = $conn->query($sql);

    unset($_SESSION["cart_present"]);

    //controllo dell'effettica cancellazione
    if (!$deletecartquery) {
      ?><script type="text/javascript">
        alert("Failed delete.");
        </script><?php
    }
  }
}



?>
