<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';

require 'inc/config.php';
require 'inc/db_connection.php';
require 'inc/secure_session.php';

secure_session_start();

if (check_login()) {
  header("location: index.php");
  exit;
}

if(isset($_POST['email'])) {
   $email = $_POST['email'];


   $sql = "SELECT * FROM users WHERE email = '$email'";
   $userquery = $conn->query($sql);
   if ($userquery && $userquery->num_rows > 0) {  // esiste l'utente

     $newtoken = getToken(100);
     $sql = "UPDATE users SET remember_token = '$newtoken' WHERE email = '$email'";
     $userquery2 = $conn->query($sql);
     if ($userquery2) {  // se l'ha settato correttamente

       //mando l'email

       $mail = new PHPMailer(true);                                // Passing `true` enables exceptions
       try {
           //Server settings
     //      $mail->SMTPDebug = 4;                                 // Enable verbose debug output
           $mail->isSMTP();                                        // Set mailer to use SMTP
           $mail->Host = 'smtp.gmail.com';                         // Specify main and backup SMTP servers
           $mail->SMTPAuth = true;                                 // Enable SMTP authentication
           $mail->Username = 'restoonline.fornitore@gmail.com';    // SMTP username
           $mail->Password = 'RestoonlinE--7';                     // SMTP password
           $mail->SMTPSecure = 'tls';                              // Enable TLS encryption, `ssl` also accepted
           $mail->Port = 587;
           $mail->smtpConnect(
                 array(
                     "ssl" => array(
                         "verify_peer" => false,
                         "verify_peer_name" => false,
                         "allow_self_signed" => true
                     )
                 )
             );

           //Recipients
           $mail->setFrom('restoonline.fornitore@gmail.com', 'RestoOnline Fornitore');
           $mail->addAddress($email);               // Name is optional


           //Content
           $mail->isHTML(true);                                  // Set email format to HTML
           $mail->Subject = 'Password reset - RestoOnline';
           $mail->Body    = "Codice di verifica: $newtoken";
           $mail->AltBody = "Codice di verifica: $newtoken";

           $mail->send();
           header("location: resetpassword-token.php");
         //  echo 'Message has been sent';
       //    exit;
       } catch (Exception $e) {

           echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        //   exit;
       }

     } else {
       ?> <script type="text/javascript">
        location.href = "resetpassword.php";
        alert("Errore inserimento token.");
       </script>
       <?php
     }
   } else {
     ?> <script type="text/javascript">
      location.href = "resetpassword.php";
      alert("Mail non registrata.");
     </script>
     <?php
   }


  $conn->close();
  exit;
}
$conn->close();
?>


<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Resetta la password - <?php echo "$APP_NAME"; ?></title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

  	<link href="css/style.css" rel="stylesheet">
    <link href="css/sign.css" rel="stylesheet">


  </head>

  <body>
    <?php include 'header.php'; ?>

    <div class="container">

      <form class="form-signin" method="POST" action="resetpassword.php">
        <h2 class="form-signin-heading">Resetta la password</h2>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name="email" required autofocus>
        <p></p>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Invia</button>
      </form>

    </div> <!-- /container -->
    <?php include 'footer.php'; ?>
    </body>
  </html>
