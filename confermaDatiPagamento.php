<?php
require 'inc/config.php';
require 'inc/db_connection.php';
require_once 'inc/secure_session.php';

secure_session_start();
if(!check_login()) {

  header("location: login.php");
  exit;
}
if (isset($_POST["conferma-dati-pagamento"]) && isset($_POST["opzione_pagamento"])) {
  if (!user_check_login()) {

    ?><script type="text/javascript">
     alert("Utente non loggato oppure alcuni dati di input mancanti. Riprovare.");
    </script><?php

  } else {
    $opzionepagamento = "";
    if ($_POST["opzione_pagamento"] == "contanti") {
      $opzionepagamento = "Pagamento in contanti";
    } else if ($_POST["opzione_pagamento"] == "paypal") {
      $opzionepagamento = "Pagamento con paypal";
    } else {
      // opzione pagamento non riconosciuto
      unset($_SESSION["dati_consegna"]);
      unset($_SESSION["dati_inseriti"]);
      exit;
    }

    // tutto pronto per registrare il nuovo ordine

    $name = $_SESSION["dati_consegna"]["name"];
    $phone = $_SESSION["dati_consegna"]["phone"];
    $address = $_SESSION["dati_consegna"]["address"];
    $notes = $_SESSION["dati_consegna"]["notes"];
    unset($_SESSION["dati_consegna"]);
    unset($_SESSION["dati_inseriti"]);

    /*search logged user id*/
    $email = $_SESSION["email"];
    $sql = "SELECT * FROM users WHERE email = '$email'";
    $userquery = $conn->query($sql);

    if ($userquery && $userquery->num_rows > 0) {
      $userrows = $userquery->fetch_assoc();
      $user_id = $userrows["id"];
      $now = date(DATE_W3C);

      $ordineinsert = $conn->query("INSERT INTO `orders`(`user_id`, `name`, `address`, `phone`, `notes`, `orderstate_id`, `paymentmethod`, `created_at`, `updated_at`) VALUES ('$user_id', '$name', '$address', '$phone', '$notes', 1, '$opzionepagamento', '$now', '$now')");
      if($ordineinsert) {
        // ho inserito l'ordine, controllo copio i prodotti dai cartitem in orderitem

        $sql = "SELECT id FROM orders WHERE user_id = $user_id ORDER BY created_at DESC LIMIT 1";
        $orderquery = $conn->query($sql);

        if ($orderquery && $orderquery->num_rows > 0) {
          $orderrow = $orderquery->fetch_assoc();
          $order_id = $orderrow["id"];
          $totalprice = 0.00;
          // creo una notifica all'admin
          $now = date(DATE_W3C);
          $notify_name = "Nuovo ordine!";
          $notify_desc = "E stato ricevuto un nuovo ordine numero $order_id";

          $sql = "INSERT INTO `adminnotifications`(`name`, `description`, `created_at`) VALUES ('$notify_name', '$notify_desc', '$now')";
          $insertnotif = $conn->query($sql);

          if($insertnotif) {
            $sql = "UPDATE users SET hasnotifications = 1 WHERE role_id = 2";   // set hasnotifications to admin(s)
            $updateadmin = $conn->query($sql);

            if ($updateadmin) {

              //copio i prodotti dai cartitem in orderitem
              $sql = "SELECT product_id, quantity, price * quantity as partial FROM cartitems, carts, products WHERE carts.user_id = $user_id AND cartitems.cart_id = carts.id AND products.id = cartitems.product_id";
              $itemsquery = $conn->query($sql);

              if ($itemsquery && $itemsquery->num_rows > 0) {

                // output data of each row
                while($items = $itemsquery->fetch_assoc()) {

                  $product_id = $items["product_id"];
                  $quantity = $items["quantity"];
                  $totalprice += $items["partial"];

                  $sql = "INSERT INTO orderitems(order_id, product_id, quantity) VALUES ($order_id, $product_id, $quantity)";
                  $insertorderitem = $conn->query($sql);

                  if (!$insertorderitem) {
                    ?> <script type="text/javascript">
                     location.href = "index.php";
                     alert("Errore inserimento order item.");
                    </script>
                    <?php
                  }
                }

                // aggiorno il prezzo totale dell'ordine
                $sql = "UPDATE `orders` SET totalprice = '$totalprice' WHERE id = $order_id";
                $pricequery = $conn->query($sql);

                if ($pricequery) {



                  // cancello il carrello
                  $sql = "DELETE FROM `carts` WHERE user_id = $user_id";
                  $cartquery = $conn->query($sql);

                  if ($cartquery) {
                    //eliminato
                    //unset($_SESSION["dati_consegna"]);
                    //unset($_SESSION["dati_inseriti"]);
                    ?> <script type="text/javascript">
                     alert("Ordine effettuato con successo. Riceverai una notifica quando sarà pronto.");
                     location.href = "my-orders.php";
                    </script>
                    <?php
                    exit;
                  } else {
                    debug_to_console($conn->error);
                    ?> <script type="text/javascript">
                     //location.href = "index.php";
                     alert("Errore cancellazione carrello.");
                    </script>
                    <?php
                  }
                } else {
                  debug_to_console($conn->error);
                ?> <script type="text/javascript">
                 //location.href = "index.php";
                 alert("Errore update order price.");
                </script>
                <?php
                }
              } else {
                debug_to_console($conn->error);
              ?> <script type="text/javascript">
               //location.href = "index.php";
               alert("Errore ricerca cart item.");
              </script>
              <?php
              }

            }else {
              debug_to_console($conn->error);
            ?> <script type="text/javascript">
             //location.href = "index.php";
             alert("Admin hasnotification set failed.");
            </script>
            <?php
          }
        } else {
debug_to_console($conn->error);
        ?> <script type="text/javascript">
         //location.href = "index.php";
         alert("Admin notification insert failed.");
        </script>
        <?php
      }



          } else {
            debug_to_console($conn->error);
          ?> <script type="text/javascript">
           //location.href = "index.php";
           alert("Errore ricerca id ordine.");
          </script>
          <?php
        }
      } else {
        debug_to_console($conn->error);
          ?> <script type="text/javascript">
           location.href = "index.php";
           //alert("Errore inserimento ordine.");
          </script>
          <?php
      }
    } else {
        ?> <script type="text/javascript">
         location.href = "index.php";
         alert("Utente non trovato.");
        </script>
        <?php
    }
    $conn->close();
  }
}


if (isset($_SESSION["dati_inseriti"]) && $_SESSION["dati_inseriti"] === true && user_check_login()  && isset($_SESSION["dati_consegna"])) {
  // doppio check dei dati
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Conferma i dati per la consegna </title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">

  	<link href="css/style.css" rel="stylesheet">
    <link href="css/sign.css" rel="stylesheet">
    <script src="js/optPagamento.js"></script>
    <script src="js/updateNotifications.js"></script>


  </head>

  <body>
    <?php include 'header.php'; ?>

    <div class="container-fluid">
      <div class="panel panel-default">
        <div class="panel-body">

          <form class="form-signin" method="POST" action="confermaDatiPagamento.php">
            <fieldset>
              <legend>
                <h2 class="form-signin-heading">Scegli un metodo di pagamento</h2>
              </legend>


              <div class="form-check">
                <input class="form-check-input" type="radio" name="opzione_pagamento"
                  id="contanti" value="contanti" checked required>
                <label class="form-check-label" for="contanti">
                  Pagamento in contanti
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="opzione_pagamento"
                  id="paypal" value="paypal" required>
                <label class="form-check-label" for="paypal">
                  PayPal
                </label>
              </div>

            </fieldset>
            <div class="ordina-button">
              <button class="btn btn-lg btn-outline-primary btn-block" type="submit" name="conferma-dati-pagamento">Procedi</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <?php include 'footer.php'; ?>
    </body>
  </html>
<?php

} else {
  header("location: index.php");
  exit;
}
