<?php
require 'inc/config.php';
require 'inc/db_connection.php';
require_once 'inc/secure_session.php';
secure_session_start();

if(!check_login()) {
  header("location: login.php");
  exit;
}
if(admin_check_login()) {
  header("location: admin/index.php");
  exit;
} else {
?>

<!DOCTYPE html>
<html lang="it">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

	<title><?php echo "$APP_NAME"; ?></title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <script src="js/cart.js"></script>
  <script src="js/notifiche.js"></script>
  <script src="js/updateNotifications.js"></script>

	<link href="css/style.css" rel="stylesheet">
</head>
<body>
<?php
  include 'header.php';
?>


<!-- Pannello categorie di lato -->
	<div class="container-fluid">
    <div class="row">
      <div class="col-sm-3 col-md-2 col-lg-2">

        <ul class="list-group">
          <li class="list-group-item active">
            <div class="flex-container">
              <div style="flex-grow: 20" >Categorie</div>
            </div>
          </li>
          <?php
    			$sql = "SELECT id, name FROM categories";
    			$result = $conn->query($sql);

    			if ($result->num_rows > 0) {
    				// output data of each row
    				while($category = $result->fetch_assoc()) { ?>


              <li class="list-group-item">
                <div class="flex-container">
                  <div style="flex-grow: 20" >
                    <a style="color: inherit" href="#<?php echo rawurlencode($category['name']);?>"><?php echo $category['name'];?></a>
                  </div>
                </div>
              </li>

              <?php
            }
          } ?>
        </ul>
      </div>
<!-- Pannello prodotti -->
      <div class="col-sm-9 col-md-6 col-lg-6">
        <ul class="list-group">

        <?php
				$sql = "SELECT id, name FROM categories";
				$cats = $conn->query($sql);

				if ($cats->num_rows > 0) {
					// output data of each row
					while($category = $cats->fetch_assoc()) { ?>


            <li class="list-group-item active">
              <div class="flex-container">
                <div style="flex-grow: 20" id="<?php echo rawurlencode($category['name']);?>"><?php echo $category['name'];?></div>
              </div>
            </li>



						<?php
						$cat_id = $category["id"];
						$sql = "SELECT * FROM `products` WHERE category_id = $cat_id";
						$prods = $conn->query($sql);

						if ($prods->num_rows <= 0) { ?>
              <li class="list-group-item">
                <div class="flex-container">
                  <div style="flex-grow: 20">Nessun prodotto per questa categoria.</div>
                </div>
              </li>

						<?php
						} else {
							// output data of each row
							while($product = $prods->fetch_assoc()) { ?>


        <li class="list-group-item">
          <div class="flex-container" style="display:flex">
            <div class="flex-item product-name" style="flex-grow: 20"><?php echo $product["name"];?></div>
            <div class="flex-item product-price" ><?php echo "€ ".$product["price"];?></div>
            <div class="flex-item add-to-cart" >


                <form action="addToCart.php" method="post">

                  <button class="btn btn-secondary add-to-cart" name="product_id" value="<?php echo $product["id"];?>">
                    <em class="fa fa-plus fa fa-fw" aria-hidden="true"></em>
                  </button>

                </form>


            </div>
          </div>
          <div class="flex-container ingredients">
          	<div class="flex-item product-name" style="flex-grow: 10;margin-right:  20%;"><?php echo $product["ingredients"];?></div>
          </div>


                </li>
							<?php
							}
						}
						?>
					<?php
					}
				}
				?>
        </ul>
      </div>


<!-- Carrello  -->
    <div class="col-sm-12 col-md-4 col-lg-4">
      <?php include 'inc/cart-panel.php'; ?>
    </div>
<!-- Fine carrello -->


  </div>

  </div>   <!-- fine container fluid-->


  <?php include 'footer.php'; ?>
  </body>
</html>

<?php
}
$conn->close();
 ?>
