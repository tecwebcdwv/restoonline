<?php
require 'inc/config.php';
require 'inc/db_connection.php';
require_once 'inc/secure_session.php';

secure_session_start();
if(!user_check_login()) {
  header("location: login.php");
  exit;
} else {

?>

<!DOCTYPE html>
<html lang="it">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

	<title><?php echo "$APP_NAME"; ?></title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <script src="js/cart.js"></script>
  <script src="js/notifiche.js"></script>
  <script src="js/updateNotifications.js"></script>

	<link href="css/style.css" rel="stylesheet">
</head>
<body>
<?php
  include 'header.php';
?>


<!-- Pannello categorie di lato -->
	<div class="container-fluid">
    <div class="row">

<!-- Carrello  -->
    <div class="col-sm-12 col-md-12 col-lg-12">
      <?php include 'inc/cart-panel.php'; ?>
    </div>
<!-- Fine carrello -->


    </div>

  </div>   <!-- fine container fluid-->

  <?php include 'footer.php'; ?>
  </body>
</html>

<?php
}
$conn->close();
 ?>
