<?php
require_once 'inc/config.php';
require 'inc/db_connection.php';
require 'inc/secure_session.php';

secure_session_start();
  if(isset($_POST["register"]) && isset($_POST["name"]) && isset($_POST["email"]) && isset($_POST["password"]) && isset($_POST["repeated_password"]) ) {
    if($_POST["password"] !== $_POST["repeated_password"]) {
      ?><script type="text/javascript">
       alert("Passwords don't match.");
      </script><?php
    } else {

      $email = $_POST["email"];
      $email_check = $conn->query("SELECT * FROM users where email = '$email'");
      if($email_check && $email_check->num_rows > 0) {
        // ho già un utente registrato con la mail
          ?> <script type="text/javascript">
           location.href = "register.php";
           alert("Email già in uso da un altro utente.");
          </script>
          <?php
      } else {

        $name=$_POST["name"];
        $email=$_POST["email"];
        $userpassword = password_hash($_POST["password"], PASSWORD_BCRYPT);
        $now = date(DATE_W3C);

        $sql="INSERT INTO `users` (name, email, password, role_id, remember_token, created_at, updated_at)
                VALUES ('$name', '$email', '$userpassword', '1', NULL, '$now', '$now');";
          if ($conn->query($sql) === TRUE) {
              echo "<script>location.href='index.php';alert('Registrazione avvenuta correttamente.');</script>";
          } else {

              echo '<script>alert("' . $conn->error . '");</script>';
              die();
          }
        }
        $conn->close();
      }
  }

?>

<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Registrati a <?php echo "$APP_NAME"; ?></title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">

  	<link href="css/style.css" rel="stylesheet">
    <link href="css/sign.css" rel="stylesheet">


  </head>

  <body>
    <?php include 'header.php'; ?>

    <div class="container">

      <form class="form-signin" method="POST" action="register.php">
        <h2 class="form-signin-heading">Registrati</h2>
        <label for="inputName" class="sr-only">Nome</label>
        <input type="text" id="inputName" class="form-control" placeholder="Nome" name="name" required autofocus>
        <label for="inputEmail" class="sr-only">Email</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Email" name="email" required>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>
        <label for="inputPasswordRepeat" class="sr-only">Ripeti Password</label>
        <input type="password" id="inputPasswordRepeat" class="form-control" placeholder="Ripeti Password" name="repeated_password" required>

        <button class="btn btn-lg btn-primary btn-block" type="submit" name="register">Registrati</button>

      </form>

    </div>
    <?php include 'footer.php'; ?>
    </body>
  </html>
