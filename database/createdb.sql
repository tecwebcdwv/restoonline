
CREATE TABLE IF NOT EXISTS `roles` (
	`id`	integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`name`	varchar(40) NOT NULL
);

CREATE TABLE IF NOT EXISTS `users` (
	`id`	integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`name`	varchar(40) NOT NULL,
	`email`	varchar(40) NOT NULL,
	`password`	varchar(255) NOT NULL,
	`role_id`	integer NOT NULL,
	`remember_token`	varchar(100),
	`created_at`	datetime,
	`updated_at`	datetime,
	`hasnotifications`	integer NOT NULL,
	FOREIGN KEY(`role_id`) REFERENCES `roles`(`id`)
);

CREATE TABLE IF NOT EXISTS `usernotifications` (
	`id`	integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`name`	varchar(40) NOT NULL,
	`description`	varchar(255) NOT NULL,
	`created_at`	datetime,
	`user_id`	integer NOT NULL,
FOREIGN KEY(`user_id`) REFERENCES `users`(`id`) on delete cascade
);

CREATE TABLE IF NOT EXISTS `adminnotifications` (
	`id`	integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`name`	varchar(40) NOT NULL,
	`description`	varchar(255) NOT NULL,
	`created_at`	datetime
);

CREATE TABLE IF NOT EXISTS `categories` (
	`id`	integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`name`	varchar(40) NOT NULL
);

CREATE TABLE IF NOT EXISTS `products` (
	`id`	integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`name`	varchar(100) NOT NULL,
	`ingredients`	varchar(255) NOT NULL,
	`category_id`	integer NOT NULL,
	`price`		decimal(10,2) NOT NULL,
	FOREIGN KEY(`category_id`) REFERENCES `categories`(`id`) on delete cascade
);

CREATE TABLE IF NOT EXISTS `orderstates` (
	`id`	integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`name`	varchar(40) NOT NULL
);

CREATE TABLE IF NOT EXISTS `orders` (
	`id`	integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`user_id`	integer NOT NULL,
	`name`	varchar(255) NOT NULL,
	`address`	varchar(255) NOT NULL,
	`phone`	varchar(255) NOT NULL,
	`notes`	varchar(255) NOT NULL,
	`orderstate_id`	integer NOT NULL,
	`totalprice`		decimal(10,2) NOT NULL,
	`paymentmethod`	varchar(255) NOT NULL,
	`created_at`	datetime,
	`updated_at`	datetime,
	FOREIGN KEY(`orderstate_id`) REFERENCES `orderstates`(`id`),
	FOREIGN KEY(`user_id`) REFERENCES `users`(`id`)
);

CREATE TABLE IF NOT EXISTS `orderitems` (
	`id`	integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`order_id`	integer NOT NULL,
	`product_id`	integer NOT NULL,
	`quantity`	integer NOT NULL,
	FOREIGN KEY(`order_id`) REFERENCES `orders`(`id`) on delete cascade
);


CREATE TABLE IF NOT EXISTS `carts` (
	`id`	integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`user_id`	integer NOT NULL,
	`created_at`	datetime,
	`updated_at`	datetime,
	FOREIGN KEY(`user_id`) REFERENCES `users`(`id`) on delete cascade
);

CREATE TABLE IF NOT EXISTS `cartitems` (
	`id`	integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`cart_id`	integer NOT NULL,
	`product_id`	integer NOT NULL,
	`quantity`	integer NOT NULL,
	FOREIGN KEY(`cart_id`) REFERENCES `carts`(`id`) on delete cascade
);
CREATE UNIQUE INDEX IF NOT EXISTS `users_email_unique` ON `users` (
	`email`
);
