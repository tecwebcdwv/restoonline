INSERT INTO `roles` (id,name) VALUES (1,'utente');
INSERT INTO `roles` (id,name) VALUES (2,'admin');
INSERT INTO `orderstates` (id,name) VALUES (1,'Ordine in attesa');
INSERT INTO `orderstates` (id,name) VALUES (2,'Ordine in preparazione');
INSERT INTO `orderstates` (id,name) VALUES (3,'Ordine in consegna');
INSERT INTO `orderstates` (id,name) VALUES (4,'Ordine consegnato');

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Primi'),
(2, 'Insalate'),
(4, 'Pizze'),
(5, 'Kebab'),
(8, 'Piadine'),
(9, 'Crescioni'),
(10, 'Hamburger'),
(11, 'Piatti Vegetariani'),
(12, 'Fritti'),
(13, 'Dolci'),
(14, 'Bevande'),
(15, 'Frutta');

INSERT INTO `products` (`id`, `name`, `ingredients`, `category_id`, `price`) VALUES
(1, 'Spaghetti in bianco', 'Spaghetti senza aggiunte', 1, '6.00'),
(2, 'Spaghetti al tonno', 'Spaghetti, tonno', 1, '6.00'),
(4, 'Margherita', 'pomodoro, mozzarella', 4, '3.50'),
(5, 'Biancaneve', 'mozzarella', 4, '3.00'),
(6, 'Menu kebab', 'piatto kebab, patatine fritte, acqua 50cl', 5, '5.00'),
(12, 'Piadina Vuota', ' ', 8, '1.00'),
(13, 'Panzerotto', 'piadina, pomodoro, mozzarella', 8, '3.00'),
(14, 'Piadina con salsiccia', 'piadina, salsiccia, cipolla', 8, '4.00'),
(15, 'Piadina Tonnara', 'piadina, pomodoro, mozzarella, insalata, salsa tonnara', 8, '4.80'),
(16, 'Crescione con pomodoro e mozzarella', 'crescione, pomodoro, mozzarella', 9, '3.00'),
(17, 'Crescione Vegetariano', 'crescione, melanzane, zucchine, peperoni, cipolla, mozzarella', 9, '4.00'),
(18, 'Crescione 4 formaggi', 'crescione, gorgonzola, fontina, pecorino, mozzarella', 9, '4.00'),
(19, 'Hamburger classico', 'hamburger, bacon, pomodoro, insalata, cheddar, cipolla', 10, '6.00'),
(20, 'Hamburger montanaro', 'hamburger, speck, radicchio, noci, gorgonzola', 10, '7.00'),
(21, 'Hamburger delicato', 'hamburger, grana, rucola, crema di aceto balsamico', 10, '7.00'),
(22, 'Verdure grigliate', ' ', 11, '4.00'),
(23, 'Caprese', 'pomodoro, mozzarella, origano', 11, '4.00'),
(24, 'Parmigiana', 'pomodoro, melanzane, scaglie di grana', 11, '4.00'),
(25, 'Patatine fritte', ' ', 12, '2.00'),
(26, 'Crocchette di pollo', ' ', 12, '4.00'),
(27, 'Crocchette di patate', ' ', 12, '2.50'),
(28, 'Piadina con nutella', 'piadina, nutella', 13, '3.00'),
(29, 'Acqua naturale', '50cl acqua naturale', 14, '1.00'),
(30, 'Acqua frizzante', '50cl acqua frizzante', 14, '1.00'),
(31, 'TÃ¨ al limone', '50cl TÃ¨ al limone', 14, '1.50'),
(32, 'TÃ¨ alla pesca', '50cl TÃ¨ alla pesca', 14, '1.50'),
(33, 'Passato di verdure', ' ', 1, '6.50'),
(34, 'Spaghetti alle vongole', 'spaghetti, vongole', 1, '10.00'),
(35, 'Risotto alla Marinara', ' ', 1, '12.00'),
(36, 'Lasagne verdi al forno', ' ', 1, '6.00'),
(37, 'Insalata Alice', 'funghi freschi, sedano, formaggio e grana', 2, '6.50'),
(38, 'Insalata Primavera', 'rucola, sedano, carote, cetrioli, funghi freschi, ravanelli e finocchio', 2, '8.00'),
(39, 'Insalata greca', 'lattuga, radicchio, cetriolo, peperoni, feta, olive greche e origano', 2, '8.00'),
(40, 'Macedonia di Frutta Fresca', ' ', 15, '5.50'),
(41, 'Macedonia di Frutta Fresca con gelato', ' ', 15, '8.00'),
(42, 'Ananas', ' ', 15, '4.00'),
(43, 'Profiteroles al cioccolato', ' ', 13, '4.00'),
(44, 'Gelato alla crema', ' ', 13, '3.50'),
(45, 'Gelato misto', ' ', 13, '4.50');
