<?php
ob_start();
require 'inc/config.php';
require 'inc/db_connection.php';
require 'inc/secure_session.php';

secure_session_start();

if(isset($_POST['email'], $_POST['password'])) {
   $email = $_POST['email'];
   $password = $_POST['password']; // Recupero la password criptata.
   if(login($email, $password, $conn) == true) {
      // user logged
      if (admin_check_login()) {
        header("location: admin/index.php");
      } else {
        header("location: index.php");
      }
      exit;
   } else {
      // failed login
      ?><script type="text/javascript">
        alert("Failed login.");
        </script><?php
   }
   $conn->close();
 }

?>


<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Accedi a <?php echo "$APP_NAME"; ?></title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

  	<link href="css/style.css" rel="stylesheet">
    <link href="css/sign.css" rel="stylesheet">


  </head>

  <body>
    <?php include 'header.php'; ?>

    <div class="container">

      <form class="form-signin" method="POST" action="login.php">
        <h2 class="form-signin-heading">Accedi al sito</h2>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name="email" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>
        <div class="checkbox">
          <label for="remember-me">
            <input type="checkbox" id="remember-me" value="remember-me"> Ricordami
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Accedi</button>
        <a class="btn btn-link" href="resetpassword.php">Ho dimenticato la password?</a>
      </form>

    </div> <!-- /container -->
    <?php include 'footer.php'; ?>
    </body>
  </html>
