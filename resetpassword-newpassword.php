<?php
require_once 'inc/config.php';
require 'inc/db_connection.php';
require 'inc/secure_session.php';

secure_session_start();
if(isset($_POST["password"]) && isset($_POST["repeated_password"]) && isset($_POST["change-password"])) {

  if(isset($_SESSION["resetter-email"]) && isset($_SESSION["resetter-token"]) ) {

    $email = $_SESSION["resetter-email"];
    $token = $_SESSION["resetter-token"];

    $email_check = $conn->query("SELECT * FROM users where email = '$email' AND remember_token = '$token'");
    if($email_check && $email_check->num_rows > 0) {
      if($_POST["password"] !== $_POST["repeated_password"]) {
        ?><script type="text/javascript">
         alert("Passwords don't match.");
        </script><?php
      } else {

        $userpassword = password_hash($_POST["password"], PASSWORD_BCRYPT);
        $now = date(DATE_W3C);

        $sql="UPDATE users SET `password`='$userpassword', remember_token = null, updated_at = '$now' WHERE email = '$email'";
        if ($conn->query($sql) === TRUE) {

          unset($_SESSION["resetter-email"]);
          unset($_SESSION["resetter-token"]);

          echo "<script>location.href='index.php';alert('Password cambiata correttamente.');</script>";
          exit;
        } else {

            echo '<script>alert("' . $conn->error . '");</script>';
            die();
        }
      }
    } else {
      ?><script type="text/javascript">
       alert("Mail and token don't match.");
      </script><?php
    }
  }

  $conn->close();
  exit;
}




if(isset($_SESSION["resetter-email"]) && isset($_SESSION["resetter-token"]) ) {



  $email = $_SESSION["resetter-email"];
  $token = $_SESSION["resetter-token"];


  $email_check = $conn->query("SELECT * FROM users where email = '$email' AND remember_token = '$token'");
  if($email_check && $email_check->num_rows > 0) {

?>

<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Registrati a <?php echo "$APP_NAME"; ?></title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">

  	<link href="css/style.css" rel="stylesheet">
    <link href="css/sign.css" rel="stylesheet">


  </head>

  <body>
    <?php include 'header.php'; ?>

    <div class="container">

      <form class="form-signin" method="POST" action="resetpassword-newpassword.php">
        <h2 class="form-signin-heading">Nuova password</h2>
        <label for="inputPassword" class="sr-only">Nuova Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>
        <label for="inputPasswordRepeat" class="sr-only">Ripeti nuova Password</label>
        <input type="password" id="inputPasswordRepeat" class="form-control" placeholder="Ripeti Password" name="repeated_password" required>

        <button class="btn btn-lg btn-primary btn-block" type="submit" name="change-password">Salva</button>

      </form>

    </div>
    <?php include 'footer.php'; ?>
    </body>
  </html>


<?php

  } else {
  ?> <script type="text/javascript">
   location.href = "index.php";
   alert("Dati non validi.");
  </script>
  <?php
  }
  $conn->close();
}
 ?>
