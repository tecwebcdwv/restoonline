<?php
require '../inc/config.php';
require '../inc/db_connection.php';
require_once '../inc/secure_session.php';

secure_session_start();

if(!admin_check_login()) {
  header("location: ../index.php");
  exit;
} else {
?>
<!DOCTYPE html>
<html lang="it">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

	<title>Listino - <?php echo "$APP_NAME"; ?></title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

	<link href="../css/style.css" rel="stylesheet">
  <script src="../js/notifiche.js"></script>
  <script src="js/updateNotifications.js"></script>
  <script src="js/inputPreCompiler.js"></script>
</head>
<body>
<?php
  include '../header.php';
?>


	<div class="container-fluid">
    <div class="row">

      <div class="col-sm-5 col-md-4 col-lg-3">

        <ul class="list-group">
          <li class="list-group-item active">
            <div class="flex-container" style="display:flex">
              <div style="flex-grow: 20" >Categorie</div>
            </div>

          </li>
          <?php
    			$sql = "SELECT id, name FROM categories";
    			$result = $conn->query($sql);

    			if ($result->num_rows > 0) {
    				// output data of each row
    				while($category = $result->fetch_assoc()) { ?>


              <li class="list-group-item">
                <div class="flex-container" style="display:flex">
                  <div class="flex-item" style="flex-grow: 20" >
                    <a style="color: inherit" href="#<?php echo rawurlencode($category['name']);?>"><?php echo $category['name'];?></a>
                  </div>
                  <div class="flex-item" style="margin-right:5px">
                    <a href="deleteCategory.php?category_id=<?php echo $category['id']; ?>" class="btn btn-warning">
                      <em class="fa fa-trash-o"></em>
                    </a>
                  </div>
                  <div class="flex-item">
                    <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#ModalCat<?php echo $category['id'];?>">
                      <em class="fa fa-pencil"></em>
                    </button>
                  </div>
                </div>

                <div class="modal fade" id="ModalCat<?php echo $category['id'];?>" tabindex="-1" role="dialog" aria-labelledby="ModalLabel<?php echo $category['id'];?>" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <div class="modal-title" id="ModalLabel<?php echo $category['id'];?>">Modifica categoria <?php echo $category['name'];?></div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">


                              <form id="EditCat<?php echo $category['id'];?>" method="post" action="editCategory.php">
                                <div class="form-group">
                                  <label for="InputNomeCat<?php echo $category['id'];?>">Nome</label>
                                    <input type="hidden" name="category_id" value="<?php echo $category['id'];?>" />
                                    <input type="text" class="form-control newinput"
                                    name="new_category_name" id="InputNomeCat<?php echo $category['id'];?>" placeholder="<?php echo $category['name'];?>" required/>
                                </div>
                              </form>



                      </div>
                      <div class="modal-footer" style="display:flex">
                        <button type="submit" class="btn btn-primary" form="EditCat<?php echo $category['id'];?>">Salva</button>

                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
                      </div>

                    </div>
                  </div>
                </div>
              </li>

              <?php
            }
          }
          ?>
          <li class="list-group-item">

            <div class="flex-container" style="display:flex">
              <div class="flex-item" style="flex-grow: 20">Aggiungi categoria</div>
              <div class="flex-item">
                <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#ModalAddCat">
                  <em class="fa fa-plus"></em>
                </button>
              </div>
            </div>

            <div class="modal fade" id="ModalAddCat" tabindex="-1" role="dialog" aria-labelledby="ModalLabelAddCat" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div class="modal-title" id="ModalLabelAddCat">Aggiungi categoria</div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">


                          <form id="AddCat" method="post" action="addCategory.php">
                            <div class="form-group">
                              <label for="InputNewCat">Nome</label>
                                <input type="text" class="form-control"
                                name="new_category_name" id="InputNewCat" placeholder="Nome nuova categoria" required/>
                            </div>
                          </form>



                  </div>
                  <div class="modal-footer" style="display:flex">
                    <button type="submit" class="btn btn-primary" form="AddCat">Aggiungi</button>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
                  </div>

                </div>
              </div>
            </div>
          </li>




        </ul>
      </div>
<!-- Pannello prodotti -->
      <div class="col-sm-7 col-md-8 col-lg-9">
        <ul class="list-group">

        <?php
				$sql = "SELECT id, name FROM categories";
				$cats = $conn->query($sql);

				if ($cats->num_rows > 0) {
					// output data of each row
					while($category = $cats->fetch_assoc()) { ?>


            <li class="list-group-item active">
              <div class="flex-container" style="display:flex">
                <div class="flex-item" style="flex-grow: 20" id="<?php echo rawurlencode($category['name']);?>"><?php echo $category['name'];?></div>
              </div>

            </li>



						<?php
						$cat_id = $category["id"];
						$sql = "SELECT * FROM `products` WHERE category_id = $cat_id";
						$prods = $conn->query($sql);

						if ($prods->num_rows > 0) {
							// output data of each row
							while($product = $prods->fetch_assoc()) { ?>


        <li class="list-group-item">
          <div class="flex-container" style="display:flex">
            <div class="flex-item product-name" style="flex-grow: 20"><?php echo $product["name"];?></div>
            <div class="flex-item" style="margin-right:5px">
              <a href="deleteProduct.php?product_id=<?php echo $product['id']; ?>" class="btn btn-warning">
                <em class="fa fa-trash-o"></em>
              </a>
            </div>
            <div class="flex-item">
              <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#ModalProd<?php echo $product['id'];?>">
                <em class="fa fa-pencil"></em>
              </button>
            </div>
          </div>


          <div class="modal fade" id="ModalProd<?php echo $product['id'];?>" tabindex="-1" role="dialog" aria-labelledby="ModalProdLabel<?php echo $product['id'];?>" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <div class="modal-title" id="ModalProdLabel<?php echo $product['id'];?>">Modifica prodotto id <?php echo $product['id'];?></div>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">


                        <form id="EditProd<?php echo $product['id'];?>" method="post" action="editProduct.php">
                          <div class="form-group">

                            <input type="hidden" name="product_id" value="<?php echo $product['id'];?>" />

                            <label for="InputNomeProd<?php echo $product['id'];?>">Nome</label>
                            <input type="text" class="form-control newinput"
                              name="new_product_name" id="InputNomeProd<?php echo $product['id'];?>" placeholder="<?php echo $product['name'];?>" required/>
                          </div>
                          <div class="form-group">
                            <label for="InputIngredientiProd<?php echo $product['id'];?>">Ingredienti</label>
                            <input type="text" class="form-control newinput"
                              name="new_product_ingredients" id="InputIngredientiProd<?php echo $product['id'];?>" placeholder="<?php echo $product['ingredients'];?>" required/>
                          </div>
                          <div class="form-group">
                            <label for="InputPriceProd<?php echo $product['id'];?>">Prezzo in €</label>
                            <input type="text" class="form-control newinput"
                              name="new_product_price" id="InputPriceProd<?php echo $product['id'];?>" placeholder="<?php echo $product['price'];?>" required/>
                          </div>
                        </form>


                </div>
                <div class="modal-footer" style="display:flex">
                  <button type="submit" class="btn btn-primary" form="EditProd<?php echo $product['id'];?>">Salva</button>

                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
                </div>

              </div>
            </div>
          </div>


        </li>

							<?php
							}
						}
            ?>
        <li class="list-group-item">

          <div class="flex-container" style="display:flex">
            <div class="flex-item" style="flex-grow: 20">Aggiungi prodotto</div>
            <div class="flex-item">
              <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#ModalAddProdToCat<?php echo $category['id'];?>">
                <em class="fa fa-plus"></em>
              </button>
            </div>
            <div class="modal fade" id="ModalAddProdToCat<?php echo $category['id'];?>" tabindex="-1" role="dialog" aria-labelledby="ModalProdLabelAddToCat<?php echo $category['id'];?>" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div class="modal-title" id="ModalProdLabelAddToCat<?php echo $category['id'];?>">Aggiungi prodotto alla categoria <?php echo $category['name'];?></div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">


                    <form id="AddProdToCat<?php echo $category['id'];?>" method="post" action="addProduct.php">
                      <div class="form-group">

                        <input type="hidden" name="category_id" value="<?php echo $category['id'];?>" />

                        <label for="InputNameNewProdToCat<?php echo $category['id'];?>">Nome</label>
                        <input type="text" class="form-control"
                          name="new_product_name" id="InputNameNewProdToCat<?php echo $category['id'];?>" placeholder="Nome prodotto" required/>
                      </div>
                      <div class="form-group">
                        <label for="InputIngredientsNewProdToCat<?php echo $category['id'];?><?php echo $product['id'];?>">Ingredienti</label>
                        <input type="text" class="form-control"
                          name="new_product_ingredients" id="InputIngredientsNewProdToCat<?php echo $category['id'];?>" placeholder="Ingredienti" />
                      </div>
                      <div class="form-group">
                        <label for="InputPriceNewProdToCat<?php echo $category['id'];?>">Prezzo in €</label>
                        <input type="text" class="form-control"
                          name="new_product_price" id="InputPriceNewProdToCat<?php echo $category['id'];?>" placeholder="Prezzo" required/>
                      </div>
                    </form>



                  </div>
                  <div class="modal-footer" style="display:flex">
                    <button type="submit" class="btn btn-primary" form="AddProdToCat<?php echo $category['id'];?>">Aggiungi</button>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </li>

					<?php
					}
				}
				?>
        </ul>
      </div>


    </div>

  </div>   <!-- fine container fluid-->


  <?php include 'footer.php'; ?>
</body>
</html>

<?php
}
$conn->close();
 ?>
