<?php
require '../inc/config.php';
require '../inc/db_connection.php';
require_once '../inc/secure_session.php';

secure_session_start();

if(!admin_check_login()) {
  header("location: ../index.php");
  exit;
} else {

?>

<!DOCTYPE html>
<html lang="it">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

	<title>Ordini - <?php echo "$APP_NAME"; ?></title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

	<link href="../css/style.css" rel="stylesheet">
  <script src="../js/notifiche.js"></script>
  <script src="js/updateNotifications.js"></script>
</head>
<body>
<?php
  include '../header.php';
?>


	<div class="container-fluid">
    <div class="row">

          <div class="col-sm-12 col-md-12 col-lg-12"> <!-- Inizio Ordini -->

            <table class="table table-hover">
              <thead>
                <tr>
                  <th id="orderid" scope="col">#</th>
                  <th id="ordertime" scope="col">Data e ora</th>
                  <th id="ordername" scope="col">Nome</th>
                  <th id="orderaddress" scope="col">Indirizzo</th>
                  <th id="buttons" scope="col"></th>
                </tr>
              </thead>
              <tbody>

              <?php
              $email = $_SESSION["email"];


              $conn->query("SET lc_time_names = 'it_IT'");
              $sql = "SELECT orders.id as orderid, orderstates.id as orderstateid, `user_id`, orders.name as ordername, `address`, `phone`, `notes`, orderstates.name as orderstatename, totalprice, paymentmethod, DATE_FORMAT(orders.created_at, '%e %M %Y, %H:%m:%s') as ordertime
              FROM orders, orderstates WHERE orders.orderstate_id = orderstates.id ORDER BY orders.created_at DESC";
              $ordersquery = $conn->query($sql);

              if ($ordersquery && $ordersquery->num_rows <= 0) { ?>
                <tr>
                  <td colspan="5">Nessun ordine</td>
                </tr>
                <?php
              } else {


                while($orderrow = $ordersquery->fetch_assoc()) {
                  $order_id = $orderrow["orderid"];
                  $order_name = $orderrow["ordername"];
                  $order_address = $orderrow["address"];
                  $order_phone = $orderrow["phone"];
                  $order_notes = $orderrow["notes"];
                  $order_state = $orderrow["orderstatename"];
                  $orderstate_id = $orderrow["orderstateid"];
                  $totalprice = $orderrow["totalprice"];
                  $paymentmethod = $orderrow["paymentmethod"];
                  $ordertime = $orderrow["ordertime"];
                  $rowclass = 1; // in attesa
                  switch($orderstate_id) {

                    case 1:
                      $rowclass = "table-warning";
                      break;
                    case 2:
                      $rowclass = "table-info";
                      break;
                    case 3:
                      $rowclass = "table-success";
                      break;
                    case 4:
                      $rowclass = "table-active";
                      break;
                  }

                   ?>


                  <tr class="<?php echo $rowclass;?>">
                    <th id="head<?php echo $order_id; ?>" headers="orderid" scope="row"><?php echo $order_id; ?></th>
                    <td headers="head<?php echo $order_id; ?> ordertime"><?php echo $ordertime; ?></td>
                    <td headers="head<?php echo $order_id; ?> ordername"><?php echo $order_name; ?></td>
                    <td headers="head<?php echo $order_id; ?> orderaddress"><?php echo $order_address;?></td>
                    <td headers="head<?php echo $order_id; ?> buttons">
                      <div class="flex-item show-order">
                        <form action="showOrder.php" method="post">
                          <button type="button" class="btn btn-secondary"  data-toggle="modal" data-target="#Modal<?php echo $order_id;?>">Dettagli</button>
                        </form>
                      </div>


                    <!-- Modal order items show-->

                    <div class="modal fade" id="Modal<?php echo $order_id;?>" tabindex="-1" role="dialog" aria-labelledby="ModalLabel<?php echo $order_id;?>" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <div class="modal-title" id="ModalLabel<?php echo $order_id;?>">Dettagli ordine numero <?php echo $order_id;?></div>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <ul class="list-group">
                              <?php
                              $sql = "SELECT name, quantity FROM orderitems, products WHERE orderitems.product_id = products.id AND orderitems.order_id = $order_id";
                              $itemsquery = $conn->query($sql);

                              if ($itemsquery && $itemsquery->num_rows > 0) {
                                while($item = $itemsquery->fetch_assoc()) {
                                  $item_name = $item["name"];
                                  $item_quantity = $item["quantity"]; ?>


                                  <li class="list-group-item">
                                    <div class="flex-container cart-product" style="display:flex">
                                      <div class="flex-item product-quantity" style="flex-grow: 2"><?php echo $item_quantity."x "; ?></div>
                                      <div class="flex-item product-name" style="flex-grow: 25"><?php echo $item_name;?></div>

                                    </div>
                                  </li>

                                <?php
                                }
                                ?>
                                <li class="list-group-item">
                                  <div class="flex-container" style="display:flex">
                                    <div class="flex-item product-name" style="flex-grow: 25"><?php echo "Nominativo: ".$order_name;?></div>
                                  </div>
                                  <div class="flex-container" style="display:flex">
                                    <div class="flex-item product-name" style="flex-grow: 25"><?php echo "Indirizzo: ".$order_address;?></div>
                                  </div>
                                  <div class="flex-container" style="display:flex">
                                    <div class="flex-item product-name" style="flex-grow: 25"><?php echo "Telefono: ".$order_phone;?></div>
                                  </div>
                                  <div class="flex-container" style="display:flex">
                                    <div class="flex-item product-name" style="flex-grow: 25"><?php echo "Prezzo totale: € ".$totalprice;?></div>
                                  </div>
                                  <div class="flex-container" style="display:flex">
                                    <div class="flex-item product-name" style="flex-grow: 25"><?php echo "Tipo pagamento: ".$paymentmethod;?></div>
                                  </div>
                                  <div class="flex-container" style="display:flex">
                                    <div class="flex-item product-name" style="flex-grow: 25"><?php echo "Note: ".$order_notes;?></div>

                                  </div>
                                </li>

                                <?php
                              }
                              ?>
                            </ul>
                          </div>
                          <div class="modal-footer" style="display:flex">
                            <?php
                            if ($orderstate_id <= 1) { ?>
                            <a href="updateOrderState.php?orderid=<?php echo $order_id; ?>&state=2" class="btn btn-primary">Segna come "In preparazione"</a>
                            <?php
                          } else if ($orderstate_id <= 2) { ?>
                            <a href="updateOrderState.php?orderid=<?php echo $order_id; ?>&state=3" class="btn btn-primary">Segna come "In consegna"</a>
                            <?php
                          } else if ($orderstate_id <= 3) { ?>
                            <a href="updateOrderState.php?orderid=<?php echo $order_id; ?>&state=4" class="btn btn-primary">Segna come "Consegnato"</a>
                            <?php
                            } ?>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>

                          </div>

                        </div>
                      </div>
                    </div>
                  </td>


                    <!-- finish modal -->
                  </tr>

                <?php
              }
              ?>

          <?php
              }


              ?>


            </tbody>
          </table>








          </div><!-- Fine Ordini -->

        </div>

      </div>   <!-- fine container fluid-->


  <?php include 'footer.php'; ?>
  </body>
</html>

<?php
}
$conn->close();
 ?>
