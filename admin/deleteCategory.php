<?php
require '../inc/config.php';
require '../inc/db_connection.php';
require_once '../inc/secure_session.php';

secure_session_start();

if(!admin_check_login()) {
  header("location: ../index.php");
  exit;
}

try {
  if (isset($_GET['category_id']) ){

    $id = $_GET["category_id"];
    $sql = "DELETE FROM categories WHERE id = $id;";

    if ($conn->query($sql) === TRUE) {
      header("location: listino.php");
    } else {
      //echo "#err:" . $conn->error;
      ?><script type="text/javascript">
        alert("Failed category delete.");
        </script><?php
    }
  } else {
    echo "#err: object not defined";
  }
} catch (Exception $e) {
  echo '#Caught exception: ',  $e->getMessage(), "\n";
}
$conn->close();
?>
