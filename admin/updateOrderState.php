<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../PHPMailer/Exception.php';
require '../PHPMailer/PHPMailer.php';
require '../PHPMailer/SMTP.php';


require '../inc/config.php';
require '../inc/db_connection.php';
require_once '../inc/secure_session.php';

secure_session_start();
if(!admin_check_login()) {
  header("location: ../index.php");
  exit;
}

if (!isset($_GET["orderid"]) || !isset($_GET["state"])) {
  header("location: ./index.php");
  exit;
}


$order_id = $_GET["orderid"];
$state = $_GET["state"];

$sql = "UPDATE `orders` SET orderstate_id = $state WHERE id = $order_id";
$orderupdate = $conn->query($sql);

if ($orderupdate) {
  // ho aggiornato lo stato dell'ordine

  // mando notifica all'user
  $sql = "SELECT user_id, email FROM orders, users WHERE orders.id = $order_id AND users.id = orders.user_id";
  $orderselect = $conn->query($sql);
  if ($orderselect && $orderselect->num_rows > 0) {
    $user = $orderselect->fetch_assoc();
    $user_id = $user["user_id"];
    $user_email = $user["email"];
    $now = date(DATE_W3C);
    $notify_name = "";
    $notify_desc = "";
    switch($state) {
      case 2:
        $notify_name = "Ordine in Preparazione!";
        $notify_desc = "Il tuo ordine numero $order_id è in preparazione.";
        break;
      case 3:
        $notify_name = "Ordine in Consegna!";
        $notify_desc = "Il tuo ordine numero $order_id è in consegna.";
        break;
      case 4:
        $notify_name = "Ordine Consegnato.";
        $notify_desc = "Il tuo ordine numero $order_id è stato consegnato.";
        break;
    }

    $sql = "INSERT INTO `usernotifications`(`name`, `description`, `created_at`, `user_id`) VALUES ('$notify_name', '$notify_desc', '$now', '$user_id')";
    $updateuser = $conn->query($sql);

    if($updateuser) {
      $sql = "UPDATE users SET hasnotifications = 1 WHERE id = $user_id";
      $updateuser2 = $conn->query($sql);

      if ($updateuser2) {




        $mail = new PHPMailer(true);                                // Passing `true` enables exceptions
        try {
            //Server settings
      //      $mail->SMTPDebug = 4;                                 // Enable verbose debug output
            $mail->isSMTP();                                        // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com';                         // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                                 // Enable SMTP authentication
            $mail->Username = 'restoonline.fornitore@gmail.com';    // SMTP username
            $mail->Password = 'RestoonlinE--7';                     // SMTP password
            $mail->SMTPSecure = 'tls';                              // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;
            $mail->smtpConnect(
                  array(
                      "ssl" => array(
                          "verify_peer" => false,
                          "verify_peer_name" => false,
                          "allow_self_signed" => true
                      )
                  )
              );

            //Recipients
            $mail->setFrom('restoonline.fornitore@gmail.com', 'RestoOnline Fornitore');
            $mail->addAddress($user_email);               // Name is optional


            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $notify_name;
            $mail->Body    = $notify_desc;
            $mail->AltBody = $notify_desc;

            $mail->send();
          //  echo 'Message has been sent';
        //    exit;
        } catch (Exception $e) {
        //    echo "<p>User mail: $user_email</p>";
        //    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        //    exit;
        }

        header("location: index.php");
        exit;
      } else { // failed user update
        ?><script type="text/javascript">
          alert("Failed set hasnotification on user.");
          </script><?php
      }
    } else { // failed notification insert
      ?><script type="text/javascript">
        alert("Failed notification insert.");
        </script><?php
    }
  }else { // failed notification insert
    ?><script type="text/javascript">
      alert("Failed user search.");
      </script><?php
  }
}else {
  ?><script type="text/javascript">
    alert("Failed order update.");
    </script><?php
}

?>
