<?php
require '../inc/config.php';
require '../inc/db_connection.php';
require_once '../inc/secure_session.php';

secure_session_start();

if(!admin_check_login()) {
  header("location: ../index.php");
  exit;
}

try {
  if (isset($_POST['product_id']) &&
      isset($_POST['new_product_name']) &&
      isset($_POST['new_product_ingredients']) &&
      isset($_POST['new_product_price'])
    ){

      $id = $_POST["product_id"];
      $name = $_POST["new_product_name"];
      $ingredients = $_POST["new_product_ingredients"];
      $price = $_POST["new_product_price"];

      $sql= "UPDATE `products` SET `name` = '$name', `ingredients` = '$ingredients', `price` = $price WHERE id = $id ";

    if ($conn->query($sql) === TRUE) {
      header("location: listino.php");
    } else {
      //echo "#err:" . $conn->error;
      ?><script type="text/javascript">
        alert("Failed product edit.");
        </script><?php
    }
  } else {
    echo "#err: object not defined";
  }
} catch (Exception $e) {
  echo '#Caught exception: ',  $e->getMessage(), "\n";
}
$conn->close();
?>
