setInterval(function() {
    $.ajax({
      type:"POST",
      url: "updateNotifications.php",
      datatype:"html",
      success:function(data) {
          if(data) {
            var currentnotifications = $("#listanotifiche").html().replace(/\s/g, '');;
            var partialpath = window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1];
            if (data.replace(/\s/g, '') != currentnotifications) {
              if (partialpath == "index.php" || partialpath == "") {
                location.href = "index.php";
              } else {
                $("#listanotifiche").html(data);
                $(".notify-toggler-icon").removeClass("fa-bell-o");
                $(".notify-toggler-icon").addClass("fa-bell");
              }
            }
          } else {
            $(".notify-toggler-icon").removeClass("fa-bell");
            $(".notify-toggler-icon").addClass("fa-bell-o");
          }

      }
    });
}, 5000);//time in milliseconds
