<?php
require 'inc/config.php';
require 'inc/db_connection.php';
require_once 'inc/secure_session.php';

secure_session_start();
if(!check_login()) {

  header("location: login.php");
  exit;
}


include 'inc/geolocation_checker.php';

if (!isset($_SESSION["cart_present"]) || $_SESSION["cart_present"] !== true) {
  header("location: index.php");
  exit;
}

if (isset($_POST["conferma-dati"])) {
    if (!user_check_login() || !isset($_POST["input_name"]) || !isset($_POST["input_phone"]) || !isset($_POST["input_address"])) {

      ?><script type="text/javascript">
       alert("Utente non loggato oppure alcuni dati di input mancanti. Riprovare.");
      </script><?php

    } else {

        if ($formatted_address = check_address($_POST["input_address"])) {
          // controllo che l'indirizzo sia a cesena e contenga il numero civico
          //altrimenti la funzione mi torna false



          $_SESSION["dati_consegna"] = array();

          $_SESSION["dati_consegna"]["name"] = $_POST["input_name"];
          $_SESSION["dati_consegna"]["phone"] = $_POST["input_phone"];
          $_SESSION["dati_consegna"]["address"] = $formatted_address;   // was $_POST["input_address"];
          $_SESSION["dati_consegna"]["notes"] = "";
          if (isset($_POST["input_notes"])) {
            $_SESSION["dati_consegna"]["notes"] = $_POST["input_notes"];
          }
          $_SESSION["dati_inseriti"] = true;

          ?> <script type="text/javascript">
           location.href = "confermaDatiPagamento.php";
          </script>
          <?php
          exit;



        } else {
          ?><script type="text/javascript">
           alert("Servizio non disponibile sull'indirizzo scelto. Assicurarsi di aver digitato un'indirizzo nella città di Cesena, compreso di numero civico.");
          </script><?php
        }
    }
}


?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Conferma i dati per la consegna </title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

  	<link href="css/style.css" rel="stylesheet">
    <link href="css/sign.css" rel="stylesheet">
    <script src="js/updateNotifications.js"></script>
    <script src="js/geolocation.js"></script>


  </head>

  <body>
    <?php include 'header.php'; ?>

    <div class="container-fluid">
      <div class="panel panel-default">
        <div class="panel-body">


          <form class="form-signin" method="POST" action="confermaDati.php">
            <h2 class="form-signin-heading">Conferma i dati di consegna</h2>
            <label for="inputName" class="sr-only">Nominativo</label>
            <input type="text" id="inputName" class="form-control" placeholder="Nominativo" name="input_name" required>

            <label for="inputPhone" class="sr-only">Telefono</label>
            <input type="tel" id="inputPhone" class="form-control" placeholder="Telefono" name="input_phone" required>

            <div class="input-group">
              <label for="inputAddress" class="sr-only">Indirizzo</label>
              <input type="text" id="inputAddress" class="form-control" placeholder="Indirizzo" name="input_address" required>
              <div class="input-group-append" style="align-items: inherit">
                <button class="btn btn-outline-secondary" type="button" onclick="getLocation()">
                  <span class="fa fa-map-marker" aria-hidden="true"></span>
                </button>
              </div>
            </div>

            <label for="inputNotes" class="sr-only">Note</label>
            <input type="text" id="inputNotes" class="form-control" placeholder="Note per il fornitore" name="input_notes">


            <div class="ordina-button">
              <button class="btn btn-lg btn-outline-primary btn-block" type="submit" name="conferma-dati">Procedi</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <?php include 'footer.php'; ?>
    </body>
  </html>
